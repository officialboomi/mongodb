// Copyright (c) 2023 Boomi, Inc.
package com.boomi.connector.mongodb.constants;

/**
 * Constants used in the connector.
 */
public class DataTypes {

    /**
     * Instantiates a new data type constants.
     */
    private DataTypes() {
        throw new IllegalStateException("Utility class");
    }

    /** The Constant INTEGER. */
    public static final String INTEGER= "INTEGER";

    /** The Constant DECIMAL128. */
    public static final String DECIMAL_128 = "DECIMAL128";

    /** The Constant BOOLEAN. */
    public static final String BOOLEAN = "BOOLEAN";

    /** The Constant DOUBLE. */
    public static final String DOUBLE = "DOUBLE";

    /** The Constant LONG. */
    public static final String LONG = "LONG";

    /** The Constant NULL. */
    public static final String NULL = "NULL";

    /** The Constant OBJECTID. */
    public static final String OBJECT_ID = "OBJECTID";

    /** The Constant OBJECT. */
    public static final String OBJECT = "OBJECT";

    /** The Constant NONE. */
    public static final String NONE = "NONE";

    /** The Constant BINARYDATA. */
    public static final String BINARY_DATA = "BINARYDATA";

    /** The Constant STRING_UPPERCASE. */
    public static final String STRING = "STRING";

    /** The Constant DATE. */
    public static final String DATE = "DATE";

    /** The Constant JAVASCRIPT. */
    public static final String JAVA_SCRIPT = "JAVASCRIPT";

    /** The Constant TIMESTAMP. */
    public static final String TIMESTAMP = "TIMESTAMP";

    /** The Constant NUMBER. */
    public static final String NUMBER = "NUMBER";

    /** The Constant NUMBER. */
    public static final String NONELKSTRENULL = "NONElkstreynull";

}
